import React from 'react';
import {useStaticQuery, graphql} from "gatsby";
import { FooterWrapper, FooterSocialWrapper, FooterSocialIcons, P } from "../elements";

export const Footer = () => {

    const data = useStaticQuery(graphql`
        query {
            facebook: file(relativePath: {eq: "facebook.png"}){
                publicURL
            }
            linkedin: file(relativePath: {eq: "linkedin.png"}){
                publicURL
            }
            instagram: file(relativePath: {eq: "instagram.svg"}){
                publicURL
            }
            twitter: file(relativePath: {eq: "twitter.png"}){
                publicURL
            }
        }
    `)


    return (
        <FooterWrapper>
            <FooterSocialWrapper>
                <FooterSocialIcons>
                    <a href="https://facebook.com" target="_blank" rel="noopener noreferrer" >
                        <img src={data.facebook.publicURL} alt="iconos"/>
                    </a>
                    <a href="https://linkedin.com" target="_blank" rel="noopener noreferrer" >
                        <img src={data.linkedin.publicURL} alt="iconos"/>
                    </a>
                    <a href="https://instagram.com" target="_blank" rel="noopener noreferrer" >
                        <img src={data.instagram.publicURL} alt="iconos"/>
                    </a>
                    <a href="https://twitter.com" target="_blank" rel="noopener noreferrer" >
                        <img src={data.twitter.publicURL} alt="iconos"/>
                    </a>
                </FooterSocialIcons>
                <P size="xSmall" color="dark3"> &copy; 2020 Filup. Todos los derechos reservados</P>
            </FooterSocialWrapper>
        </FooterWrapper>
    );
};

