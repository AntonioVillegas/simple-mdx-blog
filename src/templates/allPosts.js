import React from 'react';
import { graphql } from "gatsby";
import { Container, Content, ContentCard, FeatureImage, Pagination,Seo } from "../components";
import { H1, P } from "../elements";

//SE pasa las props en pageContext
const allPosts = ( { pageContext, data } ) => {

    const { currentPage, numPages } = pageContext;
    const isFirst = currentPage === 1;
    const isLast = currentPage === numPages;
    const prevPage = currentPage - 1 === 1 ? "/" : `/${currentPage - 1}`;
    const nextPage = `${currentPage + 1}`;

    const posts = data.allMdx.edges;

    return (
        <Container>
            <Seo />
            <FeatureImage/>
            <Content>
                <H1 textAlign="center" margin="0 0 1rem 0">
                    La transformación que urge a las áreas de recursos humanos
                </H1>
                <P color="dark2" texAlignt="center">
                    Los procesos de innovación tecnológica que vivimos están en constante actualización. Así, el área de
                    Dirección de Personal tiene acceso a información de distintas áreas que puede traducir en
                    estrategias efectivas para la toma de decisiones dentro de la organización.
                </P>
                {posts.map ( post => (
                    <ContentCard
                        key={post.node.frontmatter.slug}
                        date={post.node.frontmatter.date}
                        title={post.node.frontmatter.title}
                        excerpt={post.node.frontmatter.excerpt}
                        slug={post.node.frontmatter.slug}

                    />
                ))}
            </Content>
            <Pagination
                isFirst={isFirst}
                isLast={isLast}
                prevPage={prevPage}
                nextPage={nextPage}
            />
        </Container>
    );
};
export default allPosts

export const pageQuery = graphql`
    query allPostQuery($skip: Int!, $limit: Int!){
        allMdx(sort: {fields: frontmatter___date, order: DESC}, skip: $skip, limit: $limit) {
            edges {
                node {
                    frontmatter {
                        slug
                        title
                        date(formatString: "MMMM DD, YYYY", locale: "es")
                        excerpt
                    }
                }
            }
        }
    }
`;
