
module.exports = {
  siteMetadata: {
    title: "Mi MDX Blog",
    description: "Blog utilizando Gatsby y MDX",
    url:"http://antoniolance.com",
    image:"/office.jpg",
    twitterUsername: "@AVillegas01",
    author:"Marco Garcia"
  },
  plugins: [
    {
      resolve: `gatsby-source-filesystem`,
      options:{
        name: `posts`,
        path: `${__dirname}/src/posts`
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options:{
        name: `pages`,
        path: `${__dirname}/src/pages`
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options:{
        name: `images`,
        path: `${__dirname}/src/images`
      },
    },
    `gatsby-plugin-sharp`,
    `gatsby-transformer-sharp`,
    {
      resolve: `gatsby-plugin-mdx`,
      options: {
        extensions: [`.md`, `.mdx`],
        gatsbyRemarkPlugins: [
          {
            resolve: `gatsby-remark-images`,
            options: {
              maxWidth: 1200,
            },
          }
        ]
      }
    },
    {
      resolve: 'gatsby-source-strapi',
      options: {
        apiURL: `https://blog-filup.herokuapp.com`,
        // apiURL: `http://localhost:1337`,
        queryLimit: 1000, // Default to 100
        contentTypes: ['articles','paginas','categorias']
      }
    },
    {
      resolve: `gatsby-plugin-google-fonts`,
      options: {
        fonts: [
            `roboto mono`,
            `muli\:400,400i,700,700i`,
        ],
        display: 'swap',
      }
    },
  ],
}
